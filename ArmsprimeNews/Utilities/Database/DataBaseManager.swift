//
//  DataBaseManger.swift
//  Armsprimenews
//
//  Created by webwerks on 30/01/20.
//  Copyright © 2020 hitesh. All rights reserved.
//

import UIKit
import CoreData
class DataBaseManager: NSObject {
    // MARK: - Core Data Save Articles
    func getNewsFeed(language:String)->[Article]
    {
        var articles:[Article] = []
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "NewsArticle")
        request.predicate = NSPredicate(format: "language == %@", language)
        request.returnsObjectsAsFaults = false
        guard let appDelegate = Constants.appDelegate else { return articles }
        let context = appDelegate.persistentContainer.viewContext
        do {
            let result = try context.fetch(request)
            for data in result as! [NewsArticle] {
                let articel = Article(from: data)
                articles.append(articel)
            }
        } catch {
             // error
        }
        return articles
    }
    // MARK: - Core Data get Articles
    func saveNewsFeed(articles:[Article],language:String)
    {
        for article in articles
        {
            guard let appDelegate = Constants.appDelegate else { return}
            let managedContext = appDelegate.persistentContainer.viewContext
        
            let newsEntity = NSEntityDescription.entity(forEntityName: "NewsArticle", in: managedContext)!
            let newsArticle:NewsArticle = NSManagedObject(entity: newsEntity, insertInto: managedContext) as! NewsArticle
            newsArticle.setValue(article.author ??  "", forKeyPath: "author")
            newsArticle.setValue(article.content ?? "", forKey: "content")
            newsArticle.setValue(article.descriptionField ?? "", forKeyPath: "descriptionField")
            newsArticle.setValue(article.publishedAt ?? "", forKey: "publishedAt")
            newsArticle.setValue(article.title ?? "", forKeyPath: "title")
            newsArticle.setValue(article.url ?? "", forKey: "url")
            newsArticle.setValue(article.urlToImage ?? "", forKey: "urlToImage")
            newsArticle.setValue(language, forKey: "language")
            
            
            let sourceEntity = NSEntityDescription.entity(forEntityName: "NewsSource", in: managedContext)!
            let newsSource:NewsSource = NSManagedObject(entity: sourceEntity, insertInto: managedContext) as! NewsSource
            newsSource.setValue(article.source?.id ?? "", forKey: "id")
            newsSource.setValue(article.source?.name ?? "", forKey: "name")
            newsArticle.source = newsSource
            
            do {
                try managedContext.save()
            } catch {
                // error
            }
            
        }
    }
}

//
//  Constants.swift
//  Armsprimenews
//
//  Created by webwerks on 30/01/20.
//  Copyright © 2020 hitesh. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift
import SVProgressHUD
class Constants {
    // MARK: - UIApplication AppDelegate
    static let appDelegate =  UIApplication.shared.delegate as? AppDelegate
    
    // MARK: - language Array
    static let language = ["ar", "de", "en", "es", "fr", "he", "it", "nl", "no", "pt", "ru", "se", "ud", "zh"]
    
    // MARK: - Toast Message
    func shoeToastMessage(text:String,view:UIView,direction:ToastPosition)  {
        ToastManager.shared.position = direction
        view.makeToast(text)
    }
    // MARK: - ProgressHUD
    func showProgressHUD(isShow:Bool)  {
        SVProgressHUD().defaultStyle = .dark
        if isShow
        {
            SVProgressHUD.show()
        }
        else
        {
            SVProgressHUD.dismiss()
        }
    }
    
    func showProgressHudWithProgress(progress:Float)  {
        SVProgressHUD().defaultStyle = .dark
        SVProgressHUD.showProgress(progress)
    }
}


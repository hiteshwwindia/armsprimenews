//
//  ConstantsEnum.swift
//  Armsprimenews
//
//  Created by webwerks on 30/01/20.
//  Copyright © 2020 hitesh. All rights reserved.
//

import Foundation
// MARK: - Navigations title
enum screenTitle: String {
    case newslistScreen = "News Feed"
    case imageScreen = "Unknwn Author"
}
// MARK: - Alert messsage
enum messageAlert: String {
    case noInternetConnection = "Internet connection not avaliable"
    case somethingWrong = "Sorry, something went wrong"
}
// MARK: - Internet Mode
enum currentMode: String {
    case online
    case offline
}

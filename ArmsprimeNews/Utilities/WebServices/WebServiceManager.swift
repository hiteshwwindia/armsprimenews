//
//  WebServiceManager.swift
//  Armsprimenews
//
//  Created by webwerks on 30/01/20.
//  Copyright © 2020 hitesh. All rights reserved.
//

import Foundation
import Alamofire

class WebServiceManager: NSObject {

    static let sharedInstance = WebServiceManager()
    
    // Set NetworkEnvironment
    static let networkEnviroment: NetworkEnvironment = .dev
    
    var baseURL: String {
           switch WebServiceManager.networkEnviroment {
           case .dev: return NewsAPIPath.baseurl_Dev
           case .production: return NewsAPIPath.baseurl_Prod
           }
        }
    
    // get request
    func getrequestAPI<T:Codable>(url : String, parameters : [String:Any]? = nil,  objectType : T.Type, success : @escaping (_ responseData : T) -> Void, errorBlock: @escaping (_ error : String) -> Void){
        let url = "\(baseURL)\(url)"
        Alamofire.request(url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers : nil).responseData { response in
            switch response.result {
            case .success:
                do {
                    let responsedata = try JSONDecoder().decode(T.self, from: response.data!)
                    DispatchQueue.main.async {
                        success(responsedata)
                    }
                }
                catch {
                    errorBlock(error.localizedDescription)
                }
            case .failure(let error):
                errorBlock(error.localizedDescription)
            }
        }
    }
    // Cancel Pending Request
    func invalidateAndCancelAllRequest(){
      let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
                dataTasks.forEach { $0.cancel()
            }
            uploadTasks.forEach {
                $0.cancel() }
            downloadTasks.forEach {
                $0.cancel()}
        }
    }
}

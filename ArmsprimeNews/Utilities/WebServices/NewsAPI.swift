//
//  WebServicesConstants.swift
//  Armsprimenews
//
//  Created by webwerks on 31/01/20.
//  Copyright © 2020 hitesh. All rights reserved.
//

import Foundation
// Network Environment
enum NetworkEnvironment {
    case dev
    case production
}
// Base Url
enum NewsAPIPath {
    static let baseurl_Dev = "https://newsapi.org/v2/"
    static let baseurl_Prod = "https://newsapi.org/v2/"
}
// Api List
enum NewsAPIEndPath {
   static let getNews = "everything"
}
// Api Parameter/Key
enum NewsAPIConstant {
    static let apiKey = "3145d60eaf18468ea2a92b6875f9cb51"
    static let query = "google"
    static let pageSize = "10"
}
// Api Urls
enum NewsAPI {
    static func getNewsUrl(page: Int,language:String) -> String {
        let url = "\(NewsAPIEndPath.getNews)?apiKey=\( NewsAPIConstant.apiKey)&q=\(NewsAPIConstant.query)&language=\(language)&pageSize=\(NewsAPIConstant.pageSize)&page=\(page)"
        return url
    }
}

//
//  Extensions.swift
//  ArmsprimeNews
//
//  Created by webwerks on 01/02/20.
//  Copyright © 2020 hitesh. All rights reserved.
//

import Foundation
import UIKit
extension UILabel{
    func countLabelLines() -> Int {
        var numberLines = 0
        if let text = self.text {
            let rect = CGSize(width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude)
            let labelSize = text.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: self.font!], context: nil)
            numberLines = Int(ceil(CGFloat(labelSize.height) / self.font.lineHeight))
        }
        return numberLines
    }
    func textAlignmentRight() {
        self.textAlignment = .right
    }
    func textAlignmentLeft() {
        self.textAlignment = .left
    }
    func textAlignmentCenter() {
        self.textAlignment = .center
    }
}

extension String {
    var dateFromTimestamp: Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let date = dateFormatter.date(from: self)
        return date
    }
}

extension Date {
    func formattedDate() -> String {
        let now = Date()
        let components = Calendar.autoupdatingCurrent.dateComponents([.year,
                                                                      .month,
                                                                      .weekOfYear,
                                                                      .day,
                                                                      .hour,
                                                                      .minute,
                                                                      .second],
                                                                     from: self,
                                                                     to: now)
        if let years = components.year, years > 0 {
            return "\(years) year\(years == 1 ? "" : "s") ago"
        }
        if let months = components.month, months > 0 {
            return "\(months) month\(months == 1 ? "" : "s") ago"
        }
        if let weeks = components.weekOfYear, weeks > 0 {
            return "\(weeks) week\(weeks == 1 ? "" : "s") ago"
        }
        if let days = components.day, days > 0 {
            guard days > 1 else { return "yesterday" }
            return "\(days) day\(days == 1 ? "" : "s") ago"
        }
        if let hours = components.hour, hours > 0 {
            return "\(hours) hour\(hours == 1 ? "" : "s") ago"
        }
        if let minutes = components.minute, minutes > 0 {
            return "\(minutes) minute\(minutes == 1 ? "" : "s") ago"
        }
        if let seconds = components.second, seconds > 30 {
            return "\(seconds) second\(seconds == 1 ? "" : "s") ago"
        }
        return "Just now"
    }
}

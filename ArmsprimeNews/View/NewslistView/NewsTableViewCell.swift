//
//  NewsTableViewCell.swift
//  Armsprimenews
//
//  Created by webwerks on 30/01/20.
//  Copyright © 2020 hitesh. All rights reserved.
//

import UIKit
import SDWebImage

protocol NewsTableViewCellDelegate:class {
    func showMoreText(cell:NewsTableViewCell)
    func showImage(cell: NewsTableViewCell,imageHeight:CGFloat,indexPath: IndexPath)
    func selectImage(index: Int)
}

class NewsTableViewCell: UITableViewCell {
    
    //MARK: IBOutlets
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsDescriptionLabel: UILabel!
    @IBOutlet weak var newsAuthorLabel: UILabel!
    @IBOutlet weak var newsTimeLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var imgHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: NewsTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: Configure Cell
    func configCell(article:Article?,indexPath:IndexPath,language: String){
        setTextAlignment(language: language)
        addImageTapGestureRecognizer(indexPath: indexPath)
        showFullText(article: article)
        setImageHeightConstraint(article: article, indexPath: indexPath)
        newsTitleLabel.text = article?.title ?? ""
        newsAuthorLabel.text = "By \(article?.author ?? "-")"
        newsTimeLabel.text = "\(article?.publishedAt?.dateFromTimestamp?.formattedDate() ?? "")"
        newsDescriptionLabel.text = article?.descriptionField ?? ""
        self.layoutIfNeeded()
        isMoreButtonVisible(article: article)
    }
    
    // MARK: - Custom Method
    func showFullText(article:Article?) {
        if article?.isMoreTextVisible ?? false {
            newsDescriptionLabel.numberOfLines = 0
            moreButton.isHidden = true
        }
        else {
            newsDescriptionLabel.numberOfLines = 2
            moreButton.isHidden = false
        }
    }
    
    func isMoreButtonVisible(article:Article?) {
        let lines = newsDescriptionLabel.countLabelLines()
        if lines <= 2 || article?.isMoreTextVisible ?? false {
            moreButton.isHidden = true
        }
        else {
            moreButton.isHidden = false
        }
    }
    
    func setImageHeightConstraint(article:Article?,indexPath:IndexPath) {
        if let height = article?.imageHeight {
            if height<=0 {
                self.imgHeightConstraint.constant = self.getImageHeight(image: UIImage(named: "news") ?? UIImage.init())
                loadImageAndSet(imgUrl: article?.urlToImage ?? "",indexPath: indexPath)
            }
            else {
                self.imgHeightConstraint.constant = height
                self.newsImageView.sd_setImage(with: URL(string: article?.urlToImage ?? ""), placeholderImage: UIImage(named: "news")) { (image, error, cache, url) in
                    // (image, error, cache, url)
                }
            }
        }
    }
    
    func loadImageAndSet(imgUrl: String,indexPath:IndexPath) {
        newsImageView.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "news")) { (image, error, cache, url) in
            if let newImage = image {
                let height = self.getImageHeight(image: newImage)
                self.imgHeightConstraint.constant = height
                self.newsImageView.image = newImage
                self.delegate?.showImage(cell: self,imageHeight: height, indexPath: indexPath)
            }
        }
    }
    
    func addImageTapGestureRecognizer(indexPath:IndexPath) {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTappedAction(_:)))
        newsImageView.isUserInteractionEnabled = true
        newsImageView.tag = indexPath.row
        newsImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func getImageHeight(image:UIImage) -> CGFloat {
        let imgH = image.size.height
        let imgW = image.size.width
        let ratio = CGFloat(Double(imgH)/Double(imgW))
        let imageViewW = self.newsImageView.frame.size.width
        let imageViewH = ratio*imageViewW
        return imageViewH
    }
    
    func setTextAlignment(language: String) {
        switch language.lowercased() {
        case "ar","he":
            newsTitleLabel.textAlignmentRight()
            newsDescriptionLabel.textAlignmentRight()
            newsAuthorLabel.textAlignmentRight()
            newsTimeLabel.textAlignmentRight()
        default:
            newsTitleLabel.textAlignmentCenter()
            newsDescriptionLabel.textAlignmentLeft()
            newsAuthorLabel.textAlignmentLeft()
            newsTimeLabel.textAlignmentLeft()
        }
    }
    
    // MARK: Button Action
    @IBAction func moreButtonAction(_ sender: Any) {
        delegate?.showMoreText(cell: self)
    }
    
    @objc func imageTappedAction(_ sender:AnyObject){
        self.delegate?.selectImage(index: sender.view.tag)
    }
}

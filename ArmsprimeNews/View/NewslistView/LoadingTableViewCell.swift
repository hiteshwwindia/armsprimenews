//
//  LoadingTableViewCell.swift
//  ArmsprimeNews
//
//  Created by webwerks on 31/01/20.
//  Copyright © 2020 hitesh. All rights reserved.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: Custom Method
    func showaCellActivityIndicator(ishow:Bool) {
        if ishow
        {
            activityIndicator.startAnimating()
            activityIndicator.isHidden = false
        }
        else
        {
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        }
    }
}

//
//  RootClass.swift
//  Armsprimenews
//
//  Created by webwerks on 30/01/20.
//  Copyright © 2020 hitesh. All rights reserved.
//

import Foundation

struct RootClass : Codable {
    
    let articles : [Article]?
    let status : String?
    let totalResults : Int?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        case articles = "articles"
        case status = "status"
        case totalResults = "totalResults"
        case message = "message"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        articles = try values.decodeIfPresent([Article].self, forKey: .articles)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        totalResults = try values.decodeIfPresent(Int.self, forKey: .totalResults)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
}

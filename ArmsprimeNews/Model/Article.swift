//
//  Article.swift
//  Armsprimenews
//
//  Created by webwerks on 30/01/20.
//  Copyright © 2020 hitesh. All rights reserved.
//

import Foundation
import UIKit

struct Article : Codable {
    
    let author : String?
    let content : String?
    let descriptionField : String?
    let publishedAt : String?
    let source : Source?
    let title : String?
    let url : String?
    let urlToImage : String?
    let language : String?
    var isMoreTextVisible : Bool?
    var imageHeight : CGFloat?
    enum CodingKeys: String, CodingKey {
        case author = "author"
        case content = "content"
        case descriptionField = "description"
        case publishedAt = "publishedAt"
        case source = "source"
        case title = "title"
        case url = "url"
        case urlToImage = "urlToImage"
        case language = "language"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        author = try values.decodeIfPresent(String.self, forKey: .author)
        content = try values.decodeIfPresent(String.self, forKey: .content)
        descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
        publishedAt = try values.decodeIfPresent(String.self, forKey: .publishedAt)
        source =  try values.decodeIfPresent(Source.self, forKey: .source)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        urlToImage = try values.decodeIfPresent(String.self, forKey: .urlToImage)
        language = try values.decodeIfPresent(String.self, forKey: .language)
        isMoreTextVisible = false
        imageHeight = 0.0
    }
    
    init(from article: NewsArticle) {
        author = article.author
        content = article.content
        descriptionField = article.descriptionField
        publishedAt = article.publishedAt
        title = article.title
        url = article.url
        urlToImage = article.urlToImage
        language = article.language
        isMoreTextVisible = false
        imageHeight = 0.0
        source = Source.init(from: article.source ?? NewsSource())
    }
}

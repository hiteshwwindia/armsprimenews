//
//  NewslistViewController.swift
//  Armsprimenews
//
//  Created by webwerks on 30/01/20.
//  Copyright © 2020 hitesh. All rights reserved.
//

import UIKit
import DropDown

class NewslistViewController: UIViewController {
    var articles : [Article] = []
    var language = "en"
    var page = 1
    var isLoading = false
    var isAllNewsFetch = false
    var mode:currentMode = .offline
    let refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        return refresh
    }()
    
    @IBOutlet weak var newsFeedTableView: UITableView!
    @IBOutlet weak var languageButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        loadNewsFeed()
    }
    
    // MARK: - SetUp UI
    func setUpUI() {
        self.title = screenTitle.newslistScreen.rawValue
        newsFeedTableView.rowHeight = UITableView.automaticDimension
        newsFeedTableView.estimatedRowHeight = 500
        newsFeedTableView.dataSource = self
        newsFeedTableView.delegate = self
        setLanguageButtonTitle()
        newsFeedTableView?.refreshControl = refreshControl
        refreshControl.addTarget(self,
                                 action: #selector(refreshData(_:)),
                                 for: UIControl.Event.valueChanged)
        newsFeedTableView.tableFooterView = UIView.init()
    }
    
    func setLanguageButtonTitle(){
        languageButton.setTitle(" \(language.capitalized)", for: .normal)
    }
    
    // MARK: - Custom Method
    func refreshNewsFeed(selectedLanguage:String) {
        isAllNewsFetch = false
        page = 1
        language = selectedLanguage
        articles = []
        setLanguageButtonTitle()
        WebServiceManager().invalidateAndCancelAllRequest()
        reloadnewsFeedTableView()
        loadNewsFeed()
    }
    
    func reloadnewsFeedTableView() {
        newsFeedTableView.reloadData()
    }
    
    func loadNewsFeed(showProgressHUD:Bool=true) {
        switch Reachability().connectionStatus() {
        case .offline, .unknown:
            Constants().shoeToastMessage(text: messageAlert.noInternetConnection.rawValue, view: self.view, direction: .bottom)
            mode = .offline
            page = 1
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            fetchOfflineNewsFeed()
        case .online(_):
            mode = .online
            fetchOnlineNewsFeed(showProgressHUD: showProgressHUD)
        }
    }
    
    func fetchOfflineNewsFeed() {
        mode = .offline
        self.articles = DataBaseManager().getNewsFeed(language: self.language)
        reloadnewsFeedTableView()
    }
    
    func fetchOnlineNewsFeed(showProgressHUD:Bool) {
        isLoading = true
        if showProgressHUD {
            Constants().showProgressHUD(isShow: true)
        }
        let urlPath = NewsAPI.getNewsUrl(page: page, language: language)
        WebServiceManager().getrequestAPI(url: urlPath, objectType: RootClass.self, success: { (response) in
            Constants().showProgressHUD(isShow: false)
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            if response.status?.lowercased() == "error" {
                self.isAllNewsFetch = true
                WebServiceManager().invalidateAndCancelAllRequest()
                if let message = response.message{
                    Constants().shoeToastMessage(text: message, view: self.view, direction: .center)
                }
                if self.articles.count <= 0 {
                    if (DataBaseManager().getNewsFeed(language: self.language).count > 0)
                    {
                        Constants().shoeToastMessage(text: "Showing Offline News Feed", view: self.view, direction: .bottom)
                        self.fetchOfflineNewsFeed()
                    }
                }
            }
            else {
                self.saveOfflineNews(articles: response.articles)
                for article in response.articles ?? [] {
                    self.articles.append(article)
                }
                if self.articles.count == response.totalResults {
                    self.isAllNewsFetch = true
                }
            }
            self.isLoading = false
            self.reloadnewsFeedTableView()
        }) { (error) in
            Constants().showProgressHUD(isShow: false)
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            self.isLoading  = false
            Constants().shoeToastMessage(text: error, view: self.view, direction: .bottom)
        }
    }
    
    func saveOfflineNews(articles:[Article]?) {
        guard let articles = articles else {
            return
        }
        DataBaseManager().saveNewsFeed(articles: articles, language: language)
    }
    
    // MARK: Button Actions
    @IBAction func languageButtonAction(_ sender: UIButton) {
        let dropDown = DropDown()
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.dataSource = Constants.language
        dropDown.selectionAction = {(index: Int, item: String) in
            self.refreshNewsFeed(selectedLanguage: item)
            dropDown.hide()
        }
        dropDown.show()
    }
    @objc func refreshData(_ sender: UIRefreshControl) {
        refreshNewsFeed(selectedLanguage: language)
    }
}

// MARK: - UITableView Delegate DataSource
extension NewslistViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return articles.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            let cell:NewsTableViewCell = tableView.dequeueReusableCell(withIdentifier:"NewsCell", for: indexPath) as! NewsTableViewCell
            cell.delegate = self
            cell.configCell(article: articles[indexPath.row], indexPath: indexPath, language: language)
            return cell
        }
        else
        {
            let cell:LoadingTableViewCell = tableView.dequeueReusableCell(withIdentifier:"LoadingCell", for: indexPath) as! LoadingTableViewCell
            if articles.count <= 0 {
                cell.showaCellActivityIndicator(ishow: false)
            }
            else {
                cell.showaCellActivityIndicator(ishow: isLoading)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            switch Reachability().connectionStatus() {
            case .online(_):
                if !isLoading && !isAllNewsFetch {
                    let lastItem = self.articles.count - 1
                    if indexPath.row == lastItem {
                        if mode == .offline {
                            refreshNewsFeed(selectedLanguage: language)
                        } else {
                            page = page + 1
                            loadNewsFeed(showProgressHUD: false)
                        }
                    }
                }
            case .offline,.unknown:
                print("No Internet")
            }
        }
    }
}
// MARK: - NewsTableViewCell Delegate
extension NewslistViewController: NewsTableViewCellDelegate {
    func showImage(cell: NewsTableViewCell, imageHeight: CGFloat, indexPath: IndexPath) {
        if indexPath.row <= articles.count-1 {
            articles[indexPath.row].imageHeight = imageHeight
            self.newsFeedTableView.beginUpdates()
            self.newsFeedTableView.setNeedsDisplay()
            self.newsFeedTableView.endUpdates()
        }
    }
    
    func showMoreText(cell: NewsTableViewCell) {
        let indexPath = newsFeedTableView.indexPath(for: cell)
        if let index = indexPath,index.row <= articles.count-1 {
            articles[index.row].isMoreTextVisible = true
            self.newsFeedTableView.beginUpdates()
            self.newsFeedTableView.reloadRows(at: [index], with: UITableView.RowAnimation.automatic)
            self.newsFeedTableView.endUpdates()
        }
    }
    
    func selectImage(index: Int) {
        if index <= articles.count-1 {
            let vc:NewImageViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewImageViewController") as! NewImageViewController
            vc.article = articles[index]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

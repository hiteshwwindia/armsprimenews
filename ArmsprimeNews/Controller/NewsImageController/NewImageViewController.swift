//
//  NewImageViewController.swift
//  ArmsprimeNews
//
//  Created by webwerks on 31/01/20.
//  Copyright © 2020 hitesh. All rights reserved.
//

import UIKit
import ImageScrollView
import SDWebImage
class NewImageViewController: UIViewController {
    @IBOutlet weak var imageview: ImageScrollView!
    var article:Article?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AppDelegate.AppDelegateOrientation.setOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        Constants().showProgressHUD(isShow: false)
    }
    override func viewDidAppear(_ animated: Bool) {
        showImage()
    }
    
    // MARK: SetUp UI
    func setUpUI() {
        self.title = "By \(article?.author ?? screenTitle.imageScreen.rawValue)"
        AppDelegate.AppDelegateOrientation.setOrientation(UIInterfaceOrientationMask.all, andRotateTo: UIApplication.shared.statusBarOrientation)
        imageview.setup()
    }
    
    // MARK: Custom Method
    func showImage() {
        Constants().showProgressHUD(isShow: true)
        guard let url = article?.urlToImage else {
            Constants().shoeToastMessage(text: messageAlert.somethingWrong.rawValue, view: self.view, direction: .center)
            Constants().showProgressHUD(isShow: false)
            return
        }
        SDWebImageManager.shared.loadImage(with: URL(string: url), options: [], progress: { (recived, total, urlsis) in
        }) { (image, data, error, type, result, urlis) in
            Constants().showProgressHUD(isShow: false)
            if error != nil  {
                Constants().shoeToastMessage(text: error?.localizedDescription ?? messageAlert.somethingWrong.rawValue, view: self.view, direction: .center)
            }
            else {
                self.imageview.display(image:image ?? UIImage.init(named: "news") ?? UIImage.init())
            }
        }
    }
}
